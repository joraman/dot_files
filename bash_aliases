alias diff='colordiff'
alias less='less -R'
alias rm='rm -iv'
alias mv='mv -iv'
alias cp='cp -iv'
alias mkdir='mkdir -pv'
alias myip='dig +short myip.opendns.com @resolver1.opendns.com'
alias grep='grep --exclude-dir=.git --exclude=\*.{min.js,txt,css} --color=auto'
alias ls='ls --color'
alias fixdns='sudo cp /etc/resolv.conf_BKP /etc/resolv.conf'
alias ruta_ss='sudo ip route add 172.18.0.0/16 via 192.168.10.1'
alias configss='sshfs frontweb2:/var/www/configs/ /home/jordi/server/configss'
alias vpn='sudo openvpn --config /home/jordi/config_vpn/client.ovpn'

#Exemples
#   grep --exclude-dir="Migrations"\* -nr -C 3 "atr_type" .

