#!/usr/bin/env bash

cd ~
ln -s dot_files/bash_aliases .bash_aliases
ln -s dot_files/bashrc .bashrc
ln -s dot_files/gitconfig .gitconfig
ln -s dot_files/vimrc .vimrc
ln -s dot_files/config .ssh/config
