export PATH="/home/linuxbrew/.linuxbrew/bin:/home/jordi/Postman/:$PATH"

export EDITOR=vim

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# history
HISTTIMEFORMAT="[%F %T %Z] "
export HISTTIMEFORMAT
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
# append to the history file, don't overwrite it
shopt -s histappend

# aliases
if [ -f ~/.bash_aliases ]; then
  source ~/.bash_aliases
fi

if [[ -e /usr/lib/git-core/git-sh-prompt ]]; then
    . /usr/lib/git-core/git-sh-prompt
fi

# PS1
export PS1="\$? \h [\w]\$(__git_ps1)\n\\$\[$(tput sgr0)\] "
#unset color_prompt force_color_prompt

